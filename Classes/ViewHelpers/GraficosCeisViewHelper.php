<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 01-06-2016
 * Time: 15:30
 */

namespace CeisUfro\GraficosCeis\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

class GraficosCeisViewHelper extends AbstractFormViewHelper{

    /**
     * @param string $idGrafico
     * @param string $titulo
     * @param string $tipoGrafico
     * @param string $nombreEjeX
     * @param string $nombreEjeY
     * @param string $tamano
     * @param string $etiquetasX
     * @param string $grupoDatos
     * @param string $datosServicio
     * @return string
     */
    public function render($idGrafico, $titulo, $tipoGrafico, $nombreEjeX, $nombreEjeY, $tamano, $etiquetasX, $grupoDatos, $datosServicio) {

        $directorioVista = PATH_site."typo3conf/ext/graficos_ceis/Resources/Private/Templates/Vista/";
        $rutaCompletaVista = $directorioVista.'VistaGrafico.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        $datasets = array();
        $nombresDataset = array();
        $coloresDataset = array();

        if (empty($datosServicio)) {
            $descomArreglo = "";
            foreach($grupoDatos as $objetosDatos) {
                $descomArreglo = $descomArreglo.'<h3>Nombre Grupo: '.$objetosDatos['grupoDatos']['nombreGrupo']." - Color: ".$objetosDatos['grupoDatos']['colores']." - Cadena: ".$objetosDatos['grupoDatos']['cadenaDatos'].'</h3><br>';
                $datasets[] = $objetosDatos['grupoDatos']['cadenaDatos'];
                $nombresDataset[] = $objetosDatos['grupoDatos']['nombreGrupo'];
                $coloresDataset[] = $objetosDatos['grupoDatos']['colorGrupo'];
            }
            $vista->assign("idGrafico", $idGrafico);
            $vista->assign("tituloGrafico", $titulo);
            $vista->assign("tipoGrafico", $tipoGrafico);
            $vista->assign("tamanoGrafico", $tamano);
            $vista->assign("valorX", $nombreEjeX);
            $vista->assign("valorY", $nombreEjeY);
            $vista->assign("etiquetasEjeX", $etiquetasX);
            $vista->assign("datasets", $datasets);
            $vista->assign("nombresDataset", $nombresDataset);
            $vista->assign("coloresDataset", $coloresDataset);
        } else {
            $arregloServicio = $this->recibeJsonServicio($datosServicio);

            //Se obtiene el id correspondiente al grafico
            $arregloServicioId = $arregloServicio['idGrafico'];
            error_log("Id Grafico => ".$arregloServicioId);


            $arregloServicioLabels = $arregloServicio['etiquetasEjeX'];

            //Cadena Labels eje X
            $cadenaEtiquetas = "";
            for($cont = 0; $cont < count($arregloServicioLabels); $cont++) {
                if ($cont < (count($arregloServicioLabels) -1)) {
                    $cadenaEtiquetas = $cadenaEtiquetas.$arregloServicioLabels[$cont].",";
                } else {
                    $cadenaEtiquetas = $cadenaEtiquetas.$arregloServicioLabels[$cont];
                }
            }
            //Fin cadena Labels

            //Cadena Nombres Datasets
            $arregloServicioGrupos = $arregloServicio['gruposDatos'];

            foreach ($arregloServicioGrupos as $grupos) {
                $nombresDataset[] = $grupos["nombreGrupo"];
            }
            //Fin Cadena Nombres Datasets


            //Cadena Colores Datasets
            foreach ($arregloServicioGrupos as $color) {
                $coloresDataset[] = $color["colorGrupo"];
            }

            //Fin Cadena Colores Datasets


            //Cadena Datasets
            foreach ($arregloServicioGrupos as $grupos) {
                $arregloCadenaDatos[] = implode(",",$grupos['listaDatos']);
            }
            $datasets = $arregloCadenaDatos;
            //Fin Dataset

            $vista->assign("idGrafico", $arregloServicioId);
            $vista->assign("tituloGrafico", $arregloServicio['tituloGrafico']);
            $vista->assign("tipoGrafico", $arregloServicio['tipoGrafico']);
            $vista->assign("tamanoGrafico", $arregloServicio['tamanoGrafico']);
            $vista->assign("valorX", $arregloServicio['etiquetaX']);
            $vista->assign("valorY", $arregloServicio['etiquetaY']);
            $vista->assign("etiquetasEjeX", $cadenaEtiquetas);
            $vista->assign("datasets", $datasets);
            $vista->assign("nombresDataset", $nombresDataset);
            $vista->assign("coloresDataset", $coloresDataset);
        }

        return $vista->render();
    }

    /**
     * @param string $url
     * @return array
     */
    public function recibeJsonServicio($url) {
        $json = file_get_contents($url);
        $contenido = json_decode($json,true);
        $arregloConfGrafico = $contenido;

        return $arregloConfGrafico;
    }

}