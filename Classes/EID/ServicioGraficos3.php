<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 15-03-2016
 * Time: 11:14
 */


if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;


/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');


$configuracion = array(
    "idGrafico" => "567",
    "tipoGrafico" => "Radar",
    "tituloGrafico" => "Grafico Ejemplo 3",
    "tamanoGrafico" => "80%",
    "etiquetaX" => "Meses",
    "etiquetaY" => "Valor",
    "etiquetasEjeX" => array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio"),
    "gruposDatos" => array(
        "grupo1" => array("nombreGrupo" => "Año 2014", "colorGrupo" => "239,26,26", "listaDatos" => array(234,746,267,235,674,355,180)),
        "grupo2" => array("nombreGrupo" => "Año 2015", "colorGrupo" => "56,61,213", "listaDatos" => array(935,100,672,999,234,157,673)),
        "grupo3" => array("nombreGrupo" => "Año 2016", "colorGrupo" => "37,126,35", "listaDatos" => array(111,323,64,678,44,900,345)),
        "grupo4" => array("nombreGrupo" => "Año 2017", "colorGrupo" => "203,111,18", "listaDatos" => array(50,125,378,123,100,85,267)),
        "grupo5" => array("nombreGrupo" => "Año 2018", "colorGrupo" => "134,25,224", "listaDatos" => array(105,46,350,427,130,600,420)))

);

echo json_encode($configuracion);