/**
 * Created by Javier on 08-06-2016.
 */
var factorColorRadar = function() {
    return Math.round(Math.random() * 255);
};

//Permite generar un color rgba aleatorio
var colorAleatorioRadar = function(opacity) {
    return 'rgba(' + factorColorRadar() + ',' + factorColorRadar() + ',' + factorColorRadar() + ',' + (opacity || '.3') + ')';
};


$('div.grafico_ceis').each(function() {
    var div = $(this);
    var graficoid = div.attr('data-num-grafico');

    if ($("#recibeTipoGraficoRadar"+graficoid).val() == "Radar") {
        //Toma la cadena de etiquetas para el eje x y las separa usando la coma
        var etiquetasEjeXRadar = ($("#recibeCadenaLabelsRadar"+graficoid).val()).split(",");

        var configuracionRadar = {
            type: 'radar',
            data: {
                labels: etiquetasEjeXRadar,
                datasets: []
            },
            options: {
                legend: {
                    position: 'bottom'
                },
                title: {
                    display: true,
                    text: $("#recibeTituloGraficoRadar"+graficoid).val(),
                    fontSize: 25
                },
                scale: {
                    reverse: false,
                    ticks: {
                        beginAtZero: true
                    }
                }
            }
        };

        //Permite agregar un nuevo dataset para el grafico radar
        function generarDataRadar(etiqueta, color, cadenaDatos)  {
            var nuevoDataset = {
                label: etiqueta,
                backgroundColor: "rgba(" + color + ",0.3)"  /*colorAleatorioRadar(0.5)*/,
                pointBackgroundColor: "rgba(151,187,205,1)",
                hoverPointBackgroundColor: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: cadenaDatos
            };

            configuracionRadar.data.datasets.push(nuevoDataset);
        }

        //recorre los input agegados a la vista y se extraen los valores que seran usados por la funcion generarDataRadar
        for(var x = 0; x < $("[id*=recibeDatasetRadar"+graficoid+"]").length; x++) {

            var colorGrupo = $("#recibeColorRadar" + (graficoid) + "_" + x).val();

            generarDataRadar($("#recibeNombresDatasetRadar"+ (graficoid) + "_"+x).val(), colorGrupo, ($("#recibeDatasetRadar"+ (graficoid) + "_"+x).val()).split(","));
        }

        myRadar = new Chart(document.getElementById("canvasGraficoRadar"+graficoid), configuracionRadar);
    }



});



