/**
 * Created by Javier on 06-06-16.
 */

var randomColorFactor = function() {
    return Math.round(Math.random() * 255);
};

//Permite geenra colores rgba aleatorios
var randomColor = function(opacity) {
    return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
};


$('div.grafico_ceis').each(function() {
    var div = $(this);
    var graficoid = div.attr('data-num-grafico');

    if ($("#recibeTipoGraficoBarra"+(graficoid)).val() == "Barra") {
        var etiquetasEjeXBarra = ($("#recibeCadenaLabelsBarra"+(graficoid)).val()).split(",");

        var datosGraficoBarra = {
            labels: etiquetasEjeXBarra,
            datasets: []

        };

        //Por cada grupo de datos se genera un nuevo dataset que sera incluido para el grafico
        function generarDatosBarras(etiqueta, color, cadenaDatos)  {
            var nuevoDataset = {
                label: etiqueta,
                backgroundColor: "rgba(" + color + ",0.8)"/*randomColor(0.8)*/,
                data: cadenaDatos
            };

            datosGraficoBarra.datasets.push(nuevoDataset);
        }

        //Se recorren todos los imputs agregados a vista y se usan como parametros para la funcion generaDatosBarra
        for(var y = 0; y < $("[id*=recibeDatasetBarra" + graficoid + "]").length; y++) {
            var colorBarra = $("#recibeColorBarra"+(graficoid) +"_"+ y).val();

            generarDatosBarras($("#recibeNombresDatasetBarra"+(graficoid) +"_"+y).val(), colorBarra, ($("#recibeDatasetBarra"+(graficoid) +"_"+y).val()).split(","));
        }

        /*window.onload = function() {*/
        var ctx = document.getElementById("canvasGraficoBarra"+(graficoid)).getContext("2d");
        myBar = new Chart(ctx, {
            type: 'bar',
            data: datosGraficoBarra,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 1,
                        borderColor: 'rgb(0, 0, 0, 0.5)',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                legend: {
                    position: 'top'
                },
                title: {
                    display: true,
                    text: $("#recibeTituloGraficoBarra"+(graficoid)).val(),
                    fontSize: 25
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: $("#recibeNombreYBarra"+(graficoid)).val(),
                            fontSize: 16
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: $("#recibeNombreXBarra"+(graficoid)).val(),
                            fontSize: 16
                        }
                    }]
                }
            }
        });
    }
});



/*};*/
