/**
 * Created by Javier on 08-06-2016.
 */
var factorColor = function() {
    return Math.round(Math.random() * 255);
};

//Funcion que permite generar colores aleatorios
var colorAleatorio = function(opacity) {
    return 'rgba(' + factorColor() + ',' + factorColor() + ',' + factorColor() + ',' + (opacity || '.3') + ')';
};


$('div.grafico_ceis').each(function() {
    var div = $(this);
    var graficoid = div.attr('data-num-grafico');

    if ($("#recibeTipoGraficoLinea"+(graficoid)).val() == "Linea") {
        var etiquetasEjeXLinea = ($("#recibeCadenaLabelsLinea" + (graficoid)).val()).split(",");

        var configuracionGaficoLinea = {
            type: 'line',
            lineTension: 1,
            data: {
                labels: etiquetasEjeXLinea,
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: $("#recibeTituloGraficoLinea" + (graficoid)).val(),
                    fontSize: 25
                },
                tooltips: {
                    mode: 'label',
                    callbacks: {}
                },
                hover: {
                    mode: 'dataset'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: $("#recibeNombreXLinea" + (graficoid)).val(),
                            fontSize: 16
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: $("#recibeNombreYLinea" + (graficoid)).val(),
                            fontSize: 16
                        }
                    }]
                }
            }
        };

        //Funcion que pemrite generar un nuevo dataset que sera incluido al grafico
        function generarDatosLinea(etiqueta, color, cadenaDatos) {
            var nuevoDataset = {
                label: etiqueta,
                lineTension: 0.1,
                backgroundColor: "rgba(" + color + ",0.1)",
                data: cadenaDatos,
                borderColor: "rgba(" + color + ",0.6)"
            };

            configuracionGaficoLinea.data.datasets.push(nuevoDataset);
        }

        //Se recorren todos los inputs que se encuentran en la vista y se obtienen los valores que seran incluidos como parametro en la funcion generarDatosLinea
        for (var y = 0; y < $("[id*=recibeDatasetLinea" + (graficoid) + "]").length; y++) {

            var colorLinea = $("#recibeColorLinea" + (graficoid) + "_" + y).val();

            generarDatosLinea($("#recibeNombresDatasetLinea" + (graficoid) + "_" + y).val(), colorLinea, ($("#recibeDatasetLinea" + (graficoid) + "_" + y).val()).split(","));
        }

        /*window.onload = function() {*/

        var ctx = document.getElementById("canvasGraficoLinea" + (graficoid)).getContext("2d");
        myLine = new Chart(ctx, configuracionGaficoLinea);

    }

});



