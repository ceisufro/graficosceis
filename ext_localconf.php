<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('CeisUfro.GraficosCeis', 'Content');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['servicioGraficos'] = 'EXT:graficos_ceis/Classes/EID/ServicioGraficos.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['servicioGraficos2'] = 'EXT:graficos_ceis/Classes/EID/ServicioGraficos2.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['servicioGraficos3'] = 'EXT:graficos_ceis/Classes/EID/ServicioGraficos3.php';